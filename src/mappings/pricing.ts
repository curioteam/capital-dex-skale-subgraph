/* eslint-disable prefer-const */
import { Factory, Pair, Token, Bundle } from '../types/schema'
import { BigDecimal } from '@graphprotocol/graph-ts'
import { ZERO_BD, ONE_BD, CAPITAL_DEX_FACTORY_ADDRESS } from './helpers'
import { log } from '@graphprotocol/graph-ts'
import { syncEthPricesForPair } from './core'

const WETH_ADDRESS = '0xd2aaa00700000000000000000000000000000000'
const DAI_ADDRESS = '0x32ee5d263c76fe335fef85e4c6abe70e8d5cb097'
const CSC_ADDRESS = '0x8648923e13a01f21852ce46017fe640858e0ad8c'
const WCT1_ADDRESS = '0x2bcec1888d8c8d9b5955bf9307a96bdc2122c849'
const USDC_ADDRESS = '0xb5d8201e187fd68af4b54c7662124d93506de505'
const DAI_WETH_PAIR = '0xaf5e313a145922a0328f60d99cf291d89881b4ff'

export function getEthPriceInUSD(): BigDecimal {
  let daiPair = Pair.load(DAI_WETH_PAIR) // dai is token0

  // pair has been created
  if (daiPair !== null) {
    return daiPair.token0Price
  } else {
    return ZERO_BD
  }
}

/**
 * Search through graph to find derived Eth per token.
 **/
export function findEthPerToken(token: Token, pair: Pair): BigDecimal {
  log.info('[ETH per token]: Token: {}', [token.id])
  log.info('[ETH per token]: Pair: {}', [pair.id])

  if (token.id == WETH_ADDRESS) {
    log.info('[ETH per token]: This is WETH token', [])
    return ONE_BD
  }

  if (token.id == DAI_ADDRESS || token.id == CSC_ADDRESS || token.id == USDC_ADDRESS) {
    log.info('[ETH per token]: This is a stablecoin', [])

    let bundle = Bundle.load('1')

    if (bundle !== null) {
      let decimalOne = BigDecimal.fromString('1')
      let ethPrice = bundle.ethPrice

      if (ethPrice.notEqual(ZERO_BD)) {
        return decimalOne.div(ethPrice)
      }
    }

    return ZERO_BD
  }

  if (token.id == WCT1_ADDRESS) {
    log.info('[ETH per token]: This is wCT1 token', [])

    let bundle = Bundle.load('1')

    if (bundle !== null) {
      let ethPrice = bundle.ethPrice

      if (ethPrice.notEqual(ZERO_BD)) {
        return BigDecimal.fromString('0.97').div(ethPrice)
      }
    }

    return ZERO_BD
  }

  if (token.id == pair.token0) {
    log.info('[ETH per token]: This is token 0', [])

    let token1 = Token.load(pair.token1)
    return pair.token1Price.times(token1.derivedETH as BigDecimal) // return token1 per our token * Eth per token 1
  }

  if (token.id == pair.token1) {
    log.info('[ETH per token]: This is token 1', [])

    let token0 = Token.load(pair.token0)
    return pair.token0Price.times(token0.derivedETH as BigDecimal) // return token0 per our token * ETH per token 0
  }

  log.info('[ETH per token]: Returning zero for token: {}', [token.id])

  return ZERO_BD // nothing was found return 0
}

/**
 * Accepts tokens and amounts, return tracked amount based on token whitelist
 * If one token on whitelist, return amount in that token converted to USD.
 * If both are, return average of two amounts
 * If neither is, return 0
 */
export function getTrackedVolumeUSD(
  tokenAmount0: BigDecimal,
  token0: Token,
  tokenAmount1: BigDecimal,
  token1: Token
): BigDecimal {
  let bundle = Bundle.load('1')
  let price0 = token0.derivedETH.times(bundle.ethPrice)
  let price1 = token1.derivedETH.times(bundle.ethPrice)

  return tokenAmount0
    .times(price0)
    .plus(tokenAmount1.times(price1))
    .div(BigDecimal.fromString('2'))
}

/**
 * Accepts tokens and amounts, return tracked amount based on token whitelist
 * If one token on whitelist, return amount in that token converted to USD * 2.
 * If both are, return sum of two amounts
 * If neither is, return 0
 */
export function getTrackedLiquidityUSD(
  tokenAmount0: BigDecimal,
  token0: Token,
  tokenAmount1: BigDecimal,
  token1: Token
): BigDecimal {
  let bundle = Bundle.load('1')
  let price0 = token0.derivedETH.times(bundle.ethPrice)
  let price1 = token1.derivedETH.times(bundle.ethPrice)

  log.info('[Tracked liquidity USD]: Token prices: {}, {}', [price0.toString(), price1.toString()])
  log.info('[Tracked liquidity USD]: Both are whitelist tokens, take average of both amounts', [])

  return tokenAmount0.times(price0).plus(tokenAmount1.times(price1))
}

export function updatePairPrices(): void {
  let factory = Factory.load(CAPITAL_DEX_FACTORY_ADDRESS)
  if (factory) {
    let pairs = factory.pairs;
    for (let i = 0; i < pairs.length; i++) {
      let pair = Pair.load(pairs[i])
      if (pair) {
        syncEthPricesForPair(pair as Pair)
      }
    }
  }
}
